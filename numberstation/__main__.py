import argparse
import os
import gi

from numberstation.window import NumberstationWindow

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gio

gi.require_version('Handy', '1')
from gi.repository import Handy


class NumberstationApplication(Gtk.Application):
    def __init__(self, application_id, flags, args, version):
        Gtk.Application.__init__(self, application_id=application_id, flags=flags)
        self.connect("activate", self.new_window)
        self.args = args
        self.version = version

    def new_window(self, *args):
        NumberstationWindow(self, self.args, self.version)


def main(version):
    Handy.init()

    parser = argparse.ArgumentParser(description="Numberstation TOTP authenticator")
    parser.add_argument('url', help='TOTP url to register', nargs='?')
    args = parser.parse_args()

    if os.path.isfile('numberstation/numberstation.gresource'):
        print("Using resources from cwd/numberstation")
        resource = Gio.resource_load("numberstation.gresource")
        Gio.Resource._register(resource)
    elif os.path.isfile('numberstation.gresource'):
        print("Using resources from cwd")
        resource = Gio.resource_load("numberstation.gresource")
        Gio.Resource._register(resource)

    app = NumberstationApplication("org.postmarketos.Numberstation", Gio.ApplicationFlags.FLAGS_NONE, args=args,
                                   version=version)
    app.run()


if __name__ == '__main__':
    main('development')
