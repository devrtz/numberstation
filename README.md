Numberstation
=============

A Gnome Authenticator clone. This generates 2fa tokens based on secrets installed. It registers as uri-handler for
otpauth:// urls so they can be added from Megapixels.

![screenshot of numberstation](https://brixitcdn.net/metainfo/numberstation.png)

Hub: [sr.ht/~martijnbraam/numberstation](https://sr.ht/~martijnbraam/numberstation/)

Patches go to ~martijnbraam/public-inbox@lists.sr.ht, prefix with [PATCH numberstation]

## Features

This application generates TOTP and HOTP tokens when supplied with otpauth urls or when manually added with the add
dialog. There is also an import/export functionality that can read a file with one otpauth:// url per line.